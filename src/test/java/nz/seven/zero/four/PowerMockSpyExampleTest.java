package nz.seven.zero.four;

import static org.mockito.Mockito.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Test class for PowerMockSpyExample
 * @author Meraj
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(PowerMockSpyExample.class)
public class PowerMockSpyExampleTest {
    
    @Test
    public void testGetCurrentDateAsString() {

        PowerMockSpyExample spy = PowerMockito.spy(new PowerMockSpyExample());

        String beforeMockito = spy.getCurrentDateAsString();
        Mockito.verify(spy, times(1)).getCurrentDateAsString();
        
        Mockito.when(spy.getCurrentDateAsString()).thenReturn("Test Date");
        String mockedActual = spy.getCurrentDateAsString();

        System.out.println("before is: `" + beforeMockito + "`");
        System.out.println("mockedActual is: `" + mockedActual + "`");
        Assert.assertEquals("Test Date", mockedActual);

        Mockito.verify(spy, times(2)).getCurrentDateAsString();
    }

    @Test
    public void getCurrentYearAsInt() {
        // Spy on the Class
        PowerMockSpyExample spy = PowerMockito.spy(new PowerMockSpyExample());

        int actualYear = 2019;
        int mockedYear = 2001;

        Mockito.verify(spy, never()).getCurrentYearAsInt();

        int currentYear = spy.getCurrentYearAsInt();
        Mockito.verify(spy, times(1)).getCurrentYearAsInt();
        System.out.println("Current year is: " + currentYear);
        Assert.assertEquals(actualYear, currentYear);

        Mockito.when(spy.getCurrentYearAsInt()).thenReturn(mockedYear);
        currentYear = spy.getCurrentYearAsInt();
        Mockito.verify(spy, atLeast(2)).getCurrentYearAsInt();
        System.out.println("Current year is: " + currentYear);
        Assert.assertEquals(mockedYear, currentYear);

        Mockito.verifyNoMoreInteractions(spy);

    }
}
