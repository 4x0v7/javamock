package nz.seven.zero.four;

import java.util.Date;

public class PowerMockSpyExample {
    
    public String getCurrentDateAsString() {
        return new Date().toGMTString();
    }

    public int getCurrentYearAsInt() {
        int d = new Date().getYear();
        return d + 1900;
    }
}
